
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class VotosCero extends HttpServlet {
    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        bd.votosCero();
        res.sendRedirect(res.encodeRedirectURL("VotosCero.jsp"));
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
