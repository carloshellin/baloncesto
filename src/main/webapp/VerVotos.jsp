<%@ page import ="java.util.ArrayList"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Lista de todos los jugadores y sus votos</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="resultado">
        <font size=10>
            Lista de todos los jugadores y votos
            <hr>
            <%
                ArrayList<String> votos = (ArrayList<String>) session.getAttribute("votos");
                out.println("<ul>");
                for (String voto : votos) {
                    out.println("<li>" + voto + "</li>");
                }
                out.println("</ul>");
            %>
        </font>
        <br>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>