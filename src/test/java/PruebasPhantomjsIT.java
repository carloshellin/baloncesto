import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasPhantomjsIT
{
    private static WebDriver driver=null;
    @Test
    public void tituloIndexTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }
    

    @Test
    public void votosACeroTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.id("VotosCero")).submit();
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.id("VerVotos")).submit();
        WebElement liElements = driver.findElement(By.tagName("ul"));
        List<WebElement> ListItems = liElements.findElements(By.tagName("li"));
        for (WebElement e : ListItems) {
            String[] votos = e.getText().split(" - ", 0);
            assertEquals("0", votos[1], "El numero de votos no es correcto");
            System.out.println(votos[1]);
        }
        driver.close();
        driver.quit();
    }

    @Test
    public void votarAOtroTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebElement acb = driver.findElement(By.id("Acb"));
        acb.findElement(By.name("R1")).click();
        acb.findElement(By.name("txtOtros")).sendKeys("Jugador");
        acb.submit();
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.id("VerVotos")).submit();
        WebElement liElements = driver.findElement(By.tagName("ul"));
        List<WebElement> ListItems = liElements.findElements(By.tagName("li"));
        for (WebElement e : ListItems) {
            String[] votos = e.getText().split(" - ", 0);
            if (votos[0].equals("Jugador")) {
                assertEquals("1", votos[1], "El numero de votos no es correcto");
                System.out.println(votos[1]);
                break;
            }
        }
        driver.close();
        driver.quit();
    }
}
